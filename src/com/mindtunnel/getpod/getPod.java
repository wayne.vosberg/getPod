/**
   Copyright 2018 Wayne Vosberg <wayne@mindtunnel.com>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package com.mindtunnel.getpod;

import java.io.FileOutputStream;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

public class getPod {

	/**
	 * @param url of surprise movie prediction web site
	 */
	public static void main(String[] args) {
		
		if ( args.length != 3 ) {
			System.err.println("usage: getPod <url> <output> <email>");
			System.exit(-1);
		}
		

		try {
			URL url = new URL(args[0]);
			String image = args[1]; // path to save file
			String toEmail = args[2]; // where to send the results
			
			URL base = new URL(url.getProtocol(), url.getHost(), "");

			Document doc = Jsoup.connect(url.toString()).userAgent("Mozilla").get();
			//System.out.println(doc.head());

			URL redirect = new URL(base, doc.getElementsByAttributeValue("property", "og:url").attr("content"));
			//System.out.println("redirect: " + redirect.toString());

			doc = Jsoup.connect(redirect.toString()).userAgent("Mozilla").get();
			//System.out.println(doc.body());

			String title = doc.getElementsByAttributeValue("property", "og:title").attr("content");
			String description = doc.getElementsByAttributeValue("property", "og:description").attr("content");
			URL imageUrl = new URL(base, doc.getElementsByAttributeValue("property", "og:image").attr("content"));
			
			//Open a URL Stream
			Response resultImageResponse = Jsoup.connect(imageUrl.toString()).userAgent("Mozilla").ignoreContentType(true).maxBodySize(20*1024*1024).execute();

			FileOutputStream out = (new FileOutputStream(new java.io.File(image)));
			out.write(resultImageResponse.bodyAsBytes());  // resultImageResponse.body() is where the image's contents are.
			out.close();

			try {
				//String user = System.getProperty("user.name") + "@localhost";
				String user = System.getProperty("user.name") + "@" + 
						InetAddress.getLocalHost().getCanonicalHostName();

				// Create the email message
				HtmlEmail email = new HtmlEmail();
				email.setHostName("odin.mindtunnel.com"); // assuming localhost is configured to send
				email.addTo(toEmail);
				email.setFrom(user);
				email.setSubject(title);
				email.setCharset("UTF-8");

				email.setHtmlMsg(description + "<br/>" +  redirect.toString());
				email.setTextMsg("Your email client does not support HTML messages");

				// send the email
				email.send();
			} catch (EmailException mex) {
				mex.printStackTrace();
			}
		}
		catch (Exception e) {
			System.out.println("\nException: " + e.getMessage());
			System.exit(-1);
		}

		System.exit(0);
	}
}
