#!/bin/bash
# 
# Copyright 2018 Wayne Vosberg <wayne@mindtunnel.com>
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# 	 http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 

# set DEBUG to true if you want to see stdout/stderr messages
# these will also be emailed to the user when run from cron
# DEBUG=true will stop any output from cron
DEBUG=false
if ! eval $DEBUG
then
	# redirect stdout/stderr to /dev/null
	exec 1>>/dev/null
	exec 2>&1
fi

function gset()
{
	F="file://$1"
	# these really only need to be done once, but just making sure they are set correctly
	gsettings set org.gnome.desktop.background picture-uri "$F"
	gsettings set org.gnome.desktop.background picture-uri-dark "$F"
	gsettings set org.gnome.desktop.background picture-options "zoom"
	gsettings set org.gnome.desktop.screensaver picture-uri "$F"
}


_URL="$1"
_USER="$2"
_EMAIL="$3"
_HOME="$(eval echo ~$_USER)"
_IMAGE=${_HOME}/.POD/today.jpg

if [[ ! -d ${_HOME}/.POD ]]
then
	mkdir ${_HOME}/.POD || exit
fi

if [[ "x$DISPLAY" == "x" ]]
then
	echo "DISPLAY is not set, checking for ${_HOME}/.Xdbus"
	if [[ -f "${_HOME}/.Xdbus" ]]
	then
		echo "Sourcing ${_HOME}/.Xdbus and setting gsettings."
		. "${_HOME}/.Xdbus"
		gset "${_IMAGE}"
	else
		echo "DISPLAY not set and ${_HOME}/.Xdbus does not exist. Please run $0 from the command line once"
	fi
else
	echo "Updating ${_HOME}/.Xdbus"
	touch "${_HOME}/.Xdbus"
	chmod 600 "${_HOME}/.Xdbus"
	env | grep DBUS_SESSION_BUS_ADDRESS > "${_HOME}/.Xdbus"
	echo 'export DBUS_SESSION_BUS_ADDRESS' >> "${_HOME}/.Xdbus"
	env | grep XAUTHORITY >> "${_HOME}/.Xdbus"
	echo 'export XAUTHORITY' >> "${_HOME}/.Xdbus"
	env | grep SSH_AUTH_SOCK >> ~/.Xdbus
	echo 'export SSH_AUTH_SOCK' >> ~/.Xdbus

	gset "${_IMAGE}"
fi

JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64
JBIN=${JAVA_HOME}/bin/java
${JBIN} -jar ${_HOME}/bin/getPod.jar "${_URL}" "${_IMAGE}" "${_EMAIL}"

