# getPod

## What

Use [jsoup](https://jsoup.org) to scrape the picture of the day from my favorite site and set it as my background and screensaver. 

Tested with Ubuntu 18.04 and gdm3

## Why

Because I like the surprise of a new and interesting background everyday.

## How

1. Put getPod and getPod.jar in ~/bin or modify [getPod](getPod) accordingly.
2. Add $HOME/bin to your PATH (~/.bashrc)
3. If you prefer, you can build getPod.jar youself by importing the project into [Eclipse](http://www.eclipse.org) and exporting to a runnable jar. 
4. Run `getPod <url> <user>` at least once from the command line. This will create the ~/.Xdbus file and modify the GNOME settings appropriately.
5. Add a cron entry to run the script daily. For instance, to run at 03:33 everyday, add the following using `crontab -e`:

```
# m h  dom mon dow   command
33 3 * * * /full/path/to/getPod http://photo-of-the-day/site username

```

6. The image will be placed in ~username/.POD/today.jpg.
